<?php
class Detailpaket_model extends CI_Model{
    public $id_produk;
    public $id_paket;
    public $created_at;
    public $updated_at;

    public function getByPackage()
    {
        $this->load->database();
        $detailpaket = $this->db->get("detailpaket");
        $result = $detailpaket->result();
        return json_encode($result);
    }
}