<?php
class Produk_model extends CI_Model{
    public $id_produk;
    public $nama_produk;
    public $kategori;
    public $harga;
    public $tersedia;
    public $foto_url;
    public $created_at;
    public $updated_at;

    public function getAyampotongproduk()
    {
        $this->load->database();
        $produk = $this->db->get("produk");
        $result = $produk->result();
        return json_encode($result);
    }
}