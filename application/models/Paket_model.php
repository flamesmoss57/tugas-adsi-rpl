<?php
class Paket_model extends CI_Model{
    public $id_paket;
    public $nama_paket;
    public $tersedia;
    public $best_seller;
    public $created_at;
    public $updated_at;

    public function getAll()
    {
        $this->load->database();
        $paket = $this->db->get("paket");
        $result = $paket->result();
        return json_encode($result);
    }
}